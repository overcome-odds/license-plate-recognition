# 车牌识别

## 写在最前面
项目有很多未完善的地方，我自己在写~~bug~~代码的时候也很苦恼，因为自己对yolo这个框架了解不够深入，也没有复现过相关代码，用别人的项目实现去做整合的时候出现了很多问题，代码也比较冗长，所以在阅读notebook的时候你可能会有许多的疑惑，希望你~~少喷我~~能多留言，一起分享经验，我有时间一定会加以完善

#### 介绍
- pytorch车牌识别项目，项目也是作者踩坑的过程，分为V1和V2两个版本，实现方式分别为ResNet和小型网络+CTC_loss
-------------------------------
- V1 版本略显笨重，有接近400MB的网络参数，训练速度慢，收敛困难，且单一模型只能识别固定位数，识别准确率按位置单独计算，最低接近80%，但是从整个车牌角度看，准确率较低
- V2 CTC版本借鉴了这位大佬的[LPRNet](https://github.com/sirius-ai/LPRNet_Pytorch)，具有极小的参数（只有不到2MB的权重文件！！！），更好的性能，同时支持7-8位多类型车牌识别，在车牌生成器生成的图片训练下准确率有62%左右（其实也不是很高，主要是因为生成器的噪声给的很多，后期打算去用CCPD做一个参考，进行测试，基本上可以以那个准确率为准）
-------------------------------
- 其实我也想了一下，为什么ResNet参数那么多，效果还没有CTC的版本号，目前总结下来主要有两个原因
1. ResNet会把图片resize成正方形，会损失很多原始数据信息，而CTC使用了较宽的卷积核，并将输入resize成（24， 94）可以很好的适应长方形的车牌，保留了很多原始信息
2. ResNet就是很传统的目标分类，CTC版本进行了多尺度融合，可以很好的学习到大小不同的图片细节，且使用了CTC loss作为输出，很适合这种输出序列问题

#### 计划
- [x] 模型搭建
- [x] 优化网络结构
- [x] 数据集过少，皖A车牌过多，模型泛化能力有待提高
- [x] 实现全类型车牌识别，CTC版本已经实现
- [x] 进一步提高准确度
- [x] 添加绿牌数据集
- [ ] 整合yolov5模型，做出一个完整的目标检测+车牌识别的项目
- [ ] 项目对字符的识别能力比较弱，有待改善
- ~~听说RNN可以实现全类型的车牌识别，就不知道效果如何，如果有机会就实现一下=.=（使用了CTC实现了）~~
#### 踩坑记录
- 巨坑，HW搞反了，在opencv中是（H,W），但是在pytorch中的transform是（W,H）,导致最后resize的图片与实际图片相反，网络train不出结果，收敛速度很慢
- 测试了几个数据，数字都完全没问题，字符识别出现了错误，而且错误率挺高的，我觉得可能是因为1. 字符结构本来就比数字和字母复杂；2. 生成数据集噪声过多，与实际场景有出入
#### 说明
- 本项目在colab上实现，打开jupyter即可进行查看
- CCPD有35w张图片，占用空间极大，于是我自己切分了20%出来做训练集，10%做测试集，这里把数据集分享出来[google drive](https://drive.google.com/file/d/1jF1I0I5ZCPXYlv0KdA5uZrtEoWhxWQ5A/view?usp=sharing)

#### 训练模型参数文件
- ResNet模型的最终训练参数[google drive](https://drive.google.com/file/d/10YJ6FaFBTqS1DVBDVwc909-YEdEZxzx6/view?usp=sharing)
- CTC模型的最终训练参数[google drive](https://drive.google.com/file/d/1-DpiuuP-61dB5osJUx6lu9f8DiRN8zuV/view?usp=sharing)(在CCPD[因数据集过大，只抽取了20%训练]中训练39个epoch，最终准确率达到83.9%)
- YOLOV5模型的最终训练参数[google drive](https://drive.google.com/file/d/1uyOcz3LGOqyuFV3u53hDB-564YUBF5PW/view?usp=sharing)

#### 使用的数据集
- 参考了网上的CCPD数据集，发现数据集过于庞大，且皖A车牌较多，因此使用了[车牌生成器](https://github.com/szad670401/end-to-end-for-chinese-plate-recognition)生成训练图片进行训练
- 在CTC文件中还有一个车牌生成程序，也是github中的项目，需要可自取，感谢大佬的付出~

#### 如何运行我的文件
- 项目可运行在colab环境，调用的是colab的服务器资源，**如果你看不到下面的小图标，那就说明你需要“特殊方式”上网**
##### 步骤：
1. 点击打开colab，进入我分享的项目文件<a href="https://colab.research.google.com/drive/1atlNMy9cyD88rV7TLQMTH3Bh_6jvBtLv?usp=sharing"><img src="https://colab.research.google.com/assets/colab-badge.svg" alt="Open In Colab">
2. 运行构建网络代码块   
![运行图片](https://images.gitee.com/uploads/images/2021/0522/090723_57985523_1677163.png "屏幕截图.png")  
3. 导入测试模块    
![测试代码](https://images.gitee.com/uploads/images/2021/0522/090846_8ce7394b_1677163.png "屏幕截图.png")  
4. 导入yolov5的项目包    
![运行图片](https://images.gitee.com/uploads/images/2021/0522/090646_55adcdf1_1677163.png "屏幕截图.png")   
5. 最后，运行这个看起来很变扭，一点没有美感的代码就可以实现检测了    
![输入图片说明](https://images.gitee.com/uploads/images/2021/0522/091028_77bbe132_1677163.png "屏幕截图.png")   
- 需要注意这两个参数，分别可以配置权重文件的位置和图片的位置    
![输入图片说明](https://images.gitee.com/uploads/images/2021/0522/091206_51c37d27_1677163.png "屏幕截图.png")   